const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const ApiError = require('../error/apiError');

const register = async (req, res) => {
  const {username, password} = req.body;
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();

  res.status(200).json({message: 'User created successfully'});
};

const login = async (req, res, next) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw ApiError.badRequest('Wrong username or password');
  }

  // eslint-disable-next-line camelcase
  const jwt_token = jwt.sign({
    username: user.username,
    _id: user._id,
  }, JWT_SECRET);
  res.status(200).json({message: 'Success', jwt_token: jwt_token});
};

module.exports = {login, register};
