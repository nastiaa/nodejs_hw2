const ApiError = require('../error/apiError');
const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getProfileInfo = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  res.status(200).json({
    user: {
      _id: user._id,
      username: user.username,
      createdDate: user.createdDate,
    },
  });
};

const deleteUser = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  if (!user) {
    throw ApiError.badRequest(`User with id: ${req.params.id} does not exist`);
  }
  await user.deleteOne();
  res.status(200).json({message: 'Success'});
};

const changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({_id: req.user._id});
  if (!user || !(await bcrypt.compare(oldPassword, user.password))) {
    throw ApiError.badRequest('Wrong userID or old password');
  }
  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});
  res.status(200).json({message: 'Success'});
};

module.exports = {getProfileInfo, deleteUser, changePassword};
