const ApiError = require('../error/apiError');
const {Note} = require('../models/noteModel');

const getAllNotes = async (req, res) => {
  const notes = await Note.find({userID: req.user._id});
  res.status(200).json({notes});
};

const createNewNote = async (req, res) => {
  const note = new Note({
    userID: req.user._id,
    text: req.body.text,
  });
  await note.save();

  res.status(200).json({message: 'Note has been added successfully'});
};

const getNoteById = async (req, res, next) => {
  const note = await Note.findOne({_id: req.params.id, userID: req.user._id});
  if (!note) {
    throw ApiError.badRequest(`Note with id: ${req.params.id} does not exist`);
  }
  res.status(200).json({note});
};

const updateNoteText = async (req, res) => {
  const note = await Note.findOne({_id: req.params.id, userID: req.user._id});
  if (!note) {
    throw ApiError.badRequest(`Note with id: ${req.params.id} does not exist`);
  }
  await note.updateOne({text: req.body.text});
  res.status(200).json({message: 'Success'});
};

const deleteNoteById = async (req, res) => {
  const note = await Note.findOne({_id: req.params.id, userID: req.user._id});
  if (!note) {
    throw ApiError.badRequest(`Note with id: ${req.params.id} does not exist`);
  }
  await note.deleteOne();
  res.status(200).json({message: 'Success'});
};

const setCompleted = async (req, res) => {
  const note = await Note.findOne({_id: req.params.id, userID: req.user._id});
  if (!note) {
    throw ApiError.badRequest(`Note with id: ${req.params.id} does not exist`);
  }
  await note.updateOne({completed: !note.completed});
  res.status(200).json({message: 'Success'});
};

module.exports = {
  getAllNotes,
  createNewNote,
  getNoteById,
  updateNoteText,
  deleteNoteById,
  setCompleted,
};
