const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userID: {
    type: String,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: String,
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Note = mongoose.model('Note', noteSchema);
