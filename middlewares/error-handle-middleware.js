const ApiError = require('../error/apiError');

const apiErrorHandler = (err, req, res, next) => {
  if (err instanceof ApiError) {
    res.status(err.code).json({message: err.message});
    return;
  }

  res.status(500).json({message: err.message || 'Server error'});
};

module.exports = apiErrorHandler;
