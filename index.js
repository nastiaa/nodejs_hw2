require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const notesRouter = require('./routers/notes-router');
const authRouter = require('./routers/auth-router');
const usersRouter = require('./routers/users-router');
const apiErrorHandler = require('./middlewares/error-handle-middleware');

const port = process.env.PORT || 8080;

const app = express();
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);
app.use(apiErrorHandler);

const start = async () => {
  await mongoose.connect('mongodb+srv://testUser:testUser23@cluster0.yrlbx.mongodb.net/notesapp?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};

app.listen(port, () => {
  console.log('Server has been started...');
});

start();
