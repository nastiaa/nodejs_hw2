const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('../middlewares/auth-middleware');
const {asyncWraper} = require('../helpers/asyncWraper');
const {getProfileInfo,
  deleteUser,
  changePassword,
} = require('../controllers/usersController');

router.get('/me', authMiddleware, asyncWraper(getProfileInfo));
router.delete('/me', authMiddleware, asyncWraper(deleteUser));
router.patch('/me', authMiddleware, asyncWraper(changePassword));

module.exports = router;
