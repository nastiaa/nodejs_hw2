const express = require('express');
const router = new express.Router();
const {login, register} = require('../controllers/authController');
const {asyncWraper} = require('../helpers/asyncWraper');
const {validateRegistration} = require('../middlewares/validation-middleware');

router.post('/register',
    asyncWraper(validateRegistration),
    asyncWraper(register));
router.post('/login', asyncWraper(login));

module.exports = router;
