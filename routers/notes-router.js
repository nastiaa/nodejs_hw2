const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('../middlewares/auth-middleware');
const {asyncWraper} = require('../helpers/asyncWraper');
const {validateNoteCreation} = require('../middlewares/validation-middleware');
const {
  getAllNotes,
  createNewNote,
  getNoteById,
  updateNoteText,
  deleteNoteById,
  setCompleted} = require('../controllers/notesController');


router.get('/', authMiddleware, asyncWraper(getAllNotes));
router.post('/', authMiddleware,
    asyncWraper(validateNoteCreation),
    asyncWraper(createNewNote));
router.get('/:id', authMiddleware, asyncWraper(getNoteById));
router.put('/:id', authMiddleware, asyncWraper(updateNoteText));
router.delete('/:id', authMiddleware, asyncWraper(deleteNoteById));
router.patch('/:id', authMiddleware, asyncWraper(setCompleted));

module.exports = router;
